console.log("Hello World");

//Repetition Control Structure (Loop)
	//Loops are one of the most important feature that programming must have
	//It lets us execute code repeatedly in a pre-set number or maybe forever

	//Mini Activity

	function greeting(){
		console.log("Hi, Batch 211!");
	}
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();

	//or we can just use loops

	let countNum = 10;

	while(countNum !==0){
		console.log("This is printed inside the loop: " +countNum);
		greeting()
		countNum--;
	}

	//While Loop
	/*
	A while loop takes in an expression/condition
	If the condition evaluates to true, the statements inside the code block will be executed
	*/

	/*
	Syntax:
		while(expression/condition){
			statement/code block
			final expression ++/-- (iteration)
		}

		-expression/condition - this are the unit of code that is being evaluated in our loop
		Loop will run while the condition/expression is true

		-statement/code block - code/instructions that will be executed several times

		-final expression - indicates how to advance the loop
	*/

	let count = 5;
	//while the value of count is not equal to zero
	while(count !==0){
		//the current value of count is printed out
		console.log("While: " + count);
		//decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
		count --;
	}

	/*let num = 20;

	while(num !==0) { if it is changed to 9 or more than for  decrement it will have an infinite
		console.log("While: num " + num)
		num --; //don't forget not to get a blue screen
	}
	*/

	/*let digit = 5; if it is changed to below the set number or more than for increment it will have an infinite
	while(digit !== 20){ 
		console.log("While: digit " + digit)
		digit ++;
	}*/

	/*let num1 = 1;
	while(num1 ===2){ //loop will not run because the condition did not met
		console.log("While: num1 " + num1);
		num--
	}*/

	//Do while Loop

	/*
	-A do-while loop works a lot like while loop
	But unlike while loops, do-while loops guarantee that the code will be executed at least once

	Syntax: 
	do{
		statement/code block
		finalExpression ++/--
	}while (expression/condition)
	*/

	/*
	How the Do While Loop Works;
	1. The statements in the "do" block executes once
	2. The message "Do While: " + number will be printed out in the console
	3. After executing once, the while statement will evaluate whether to run the next iteration of the loop based on the given expression/condition
	4. If the expression/condition is not true, another iteration of the loop will be executed and will be repetaed until the condition is met
	5. If the condition is true, the loop will stop
	*/

	let number = Number(prompt("Give me a number"));
	//Number function same as parseInt but number takes 1 parameter
	do{
		console.log("Do while: "+ number)
		number += 1;
	}while(number<10)

	//For Loop

	//A For Loop is more flexible than while and do while loops
	/*
	It consists of three parts:
		1. Initialization value that will track the progression of the loop
		2. Expression/Condition that will be evaluated which will determine if the loop will run one more time
		3. finalExpression indicates how to advance the loop

		Syntax:

		for (initialization; expression/condition; finalExpression){
			statement
		}
	*/


	for (let count = 0; count <=20; count++){
		console.log("For Loop: " + count);
	}

	//Mini Activity
	for (let count = 0; count <=20; count++){
		if(count % 2 == 0 && count != 0){
		/*console.log("For Loop: " + count);*/
			console.log("Even: " + count);
		}
	}

	let myString = "Camille Doroteo"; //result 15
	//characters in strings may be counted using the .length property
	//strings are special compared to other data types
	console.log(myString.length); //provide characters/letters including the same

	//Accessing elements of a string
	//individual characters of a string may be accessed using its index number
	//the first character in a string corresponds to the number 0, the next is 1...

	//index string count always starts with 0 Example: "Camille Doroteo" - [0] - C
	console.log(myString[2]); //m
	console.log(myString[0]); //c
	console.log(myString[8]); //D
	console.log(myString[14]); //o

	/*for(let x = 0; x < myString.length; x++){
		console.log(myString[x])
	}*/

	//Mini Activity

	let myString1 = "Joanna Carlos";
	for(let x = 0; x < myString.length; x++){
		console.log(myString1[x])
	}

	let myName = "NehEmIAh"; //Nhmh
	let myNewName = '';

	for (let i=0; i<myName.length; i++){
		if(
			myName[i].toLowerCase() == "a" ||
			myName[i].toLowerCase() == "i" ||
			myName[i].toLowerCase() == "u" ||
			myName[i].toLowerCase() == "e" ||
			myName[i].toLowerCase() == "o" 
	){
			console.log(3)
		}else{
			console.log(myName[i]);
			myNewName += myName[i];
		}
	}
	console.log(myNewName); //concatenate the characters based on the given conditions


	//Continue and break statements 
	/*
	The continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

	The break statement is used to terminate the current loop once a match has been found
	*/

	for(let count = 0; count <=20; count++){

		if(count % 2 === 0){
			console.log("Even Number")
			//tells the console to continue to the next iteration of the loop
			continue; //ignore all statements located after the continue statement
			/*console.log("Hello") */
		}
		console.log('Continue and Break: ' + count);

		if(count>10){
			//number values after 10 will no longer printed
			//"break" tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
			break; 
		}
	}

	let name = 'alexandro';
	for (let i = 0; i <name.length; i++){
		//the current letter is printed out based on its index
		console.log(name[i])
		if(name[i].toLowerCase() === "a"){
			console.log("Continue to the next iteration");
			continue;
		}
		if(name[i] == "d"){
			break;
		}
	}

	